<?php

namespace Metaflo\SchemoBundle\Entity;

use Doctrine\Common\Proxy\Exception\InvalidArgumentException;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\MaxDepth;

/**
 * Timeslot
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ExclusionPolicy("all")
 */
class Timeslot {
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(type="smallint")
     * @Expose
     */
    private $weekday; //TODO: validate that each weekday-timeslot combination is unique
    const DAY_MON = 1;
    const DAY_TUE = 2;
    const DAY_WED = 3;
    const DAY_THU = 4;
    const DAY_FRI = 5;
    const DAY_SAT = 6;
    const DAY_SUN = 7;
    public static $DAYS = array(
        self::DAY_MON => "Mon",
        self::DAY_TUE => "Tue",
        self::DAY_WED => "Wed",
        self::DAY_THU => "Thu",
        self::DAY_FRI => "Fri",
        self::DAY_SAT => "Sat",
        self::DAY_SUN => "Sun",
    );

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timeStart", type="time")
     * @Expose
     */
    private $timeStart;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timeEnd", type="time")
     * @Expose
     */
    private $timeEnd;

    /**
     * @var Lesson[]
     *
     * @ORM\OneToMany(targetEntity="Lesson", mappedBy="timeslot")
     */
    private $lessons;

    /**
     * @var Project
     *
     * @ORM\ManyToOne(targetEntity="Project", inversedBy="timeslots")
     */
    private $project;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }


    /**
     * Set timeStart
     *
     * @param \DateTime $timeStart
     * @return Timeslot
     */
    public function setTimeStart($timeStart) {
        $this->timeStart = $timeStart;

        return $this;
    }

    /**
     * Get timeStart
     *
     * @return \DateTime
     */
    public function getTimeStart() {
        return $this->timeStart;
    }

    /**
     * Set timeEnd
     *
     * @param \DateTime $timeEnd
     * @return Timeslot
     */
    public function setTimeEnd($timeEnd) {
        $this->timeEnd = $timeEnd;

        return $this;
    }

    /**
     * Get timeEnd
     *
     * @return \DateTime
     */
    public function getTimeEnd() {
        return $this->timeEnd;
    }

    /**
     * Constructor
     */
    public function __construct() {
        $this->lessons = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add lessons
     *
     * @param \Metaflo\SchemoBundle\Entity\Lesson $lessons
     * @return Timeslot
     */
    public function addLesson(\Metaflo\SchemoBundle\Entity\Lesson $lessons) {
        $this->lessons[] = $lessons;

        return $this;
    }

    /**
     * Remove lessons
     *
     * @param \Metaflo\SchemoBundle\Entity\Lesson $lessons
     */
    public function removeLesson(\Metaflo\SchemoBundle\Entity\Lesson $lessons) {
        $this->lessons->removeElement($lessons);
    }

    /**
     * Get lessons
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLessons() {
        return $this->lessons;
    }

    /**
     * Set project
     *
     * @param \Metaflo\SchemoBundle\Entity\Project $project
     * @return Timeslot
     */
    public function setProject(\Metaflo\SchemoBundle\Entity\Project $project = null) {
        $this->project = $project;

        return $this;
    }

    /**
     * Get project
     *
     * @return \Metaflo\SchemoBundle\Entity\Project
     */
    public function getProject() {
        return $this->project;
    }

    function __toString() {
        return $this->getWeekdayName() . " (" .
        $this->timeStart->format('H:i') . " - " .
        $this->timeEnd->format('H:i') . ")";
    }

    /**
     * Set weekday
     *
     * @param integer $weekday
     * @throws \Symfony\Component\DependencyInjection\Exception\InvalidArgumentException
     * @return Timeslot
     */
    public function setWeekday($weekday) {
        if (!array_key_exists($weekday, self::$DAYS))
            throw new \Symfony\Component\DependencyInjection\Exception\InvalidArgumentException(
                "weekday must be between 1 (Mon) and 7 (Sun)"
            );
        $this->weekday = $weekday;

        return $this;
    }

    /**
     * Get weekday
     *
     * @return integer
     */
    public function getWeekday() {
        return $this->weekday;
    }

    public function getWeekdayName() {
        return self::$DAYS[$this->weekday];
    }
}
