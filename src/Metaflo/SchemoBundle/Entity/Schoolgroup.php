<?php

namespace Metaflo\SchemoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\MaxDepth;

/**
 * Class
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ExclusionPolicy("all")
 */
class Schoolgroup {
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Expose
     */
    private $name;

    /**
     * @var LessonTemplate[]
     *
     * @ORM\OneToMany(targetEntity="LessonTemplate", mappedBy="schoolgroup")
     */
    private $lessonTemplates;

    /**
     * @var Project
     *
     * @ORM\ManyToOne(targetEntity="Project", inversedBy="schoolgroups")
     */
    private $project;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Class
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Constructor
     */
    public function __construct() {
        $this->lessonTemplates = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add lessonTemplates
     *
     * @param \Metaflo\SchemoBundle\Entity\LessonTemplate $lessonTemplates
     * @return Schoolgroup
     */
    public function addLessonTemplate(\Metaflo\SchemoBundle\Entity\LessonTemplate $lessonTemplates) {
        $this->lessonTemplates[] = $lessonTemplates;

        return $this;
    }

    /**
     * Remove lessonTemplates
     *
     * @param \Metaflo\SchemoBundle\Entity\LessonTemplate $lessonTemplates
     */
    public function removeLessonTemplate(\Metaflo\SchemoBundle\Entity\LessonTemplate $lessonTemplates) {
        $this->lessonTemplates->removeElement($lessonTemplates);
    }

    /**
     * Get lessonTemplates
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLessonTemplates() {
        return $this->lessonTemplates;
    }

    /**
     * Set project
     *
     * @param \Metaflo\SchemoBundle\Entity\Project $project
     * @return Schoolgroup
     */
    public function setProject(\Metaflo\SchemoBundle\Entity\Project $project = null) {
        $this->project = $project;

        return $this;
    }

    /**
     * Get project
     *
     * @return \Metaflo\SchemoBundle\Entity\Project
     */
    public function getProject() {
        return $this->project;
    }

    function __toString() {
        return $this->name;
    }
}
