<?php

namespace Metaflo\SchemoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\MaxDepth;

/**
 * Teacher
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ExclusionPolicy("all")
 */
class Teacher {
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Expose
     */
    private $name;

    /**
     * @var Lesson[]
     *
     * @ORM\OneToMany(targetEntity="Lesson", mappedBy="teacher")
     */
    private $lessonsAssigned;

    /**
     * @var LessonTemplate[]
     *
     * @ORM\ManyToMany(targetEntity="LessonTemplate", mappedBy="teachers")
     */
    private $lessonsPossible;

    /**
     * @var Project
     *
     * @ORM\ManyToOne(targetEntity="Project", inversedBy="teachers")
     */
    private $project;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Teacher
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set toughtCourses
     *
     * @param array $toughtCourses
     * @return Teacher
     */
    public function setToughtCourses($toughtCourses) {
        $this->toughtCourses = $toughtCourses;

        return $this;
    }

    /**
     * Get toughtCourses
     *
     * @return array
     */
    public function getToughtCourses() {
        return $this->toughtCourses;
    }

    /**
     * Constructor
     */
    public function __construct() {
        $this->lessonsAssigned = new \Doctrine\Common\Collections\ArrayCollection();
        $this->lessonsPossible = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add lessonsAssigned
     *
     * @param \Metaflo\SchemoBundle\Entity\Lesson $lessonsAssigned
     * @return Teacher
     */
    public function addLessonsAssigned(\Metaflo\SchemoBundle\Entity\Lesson $lessonsAssigned) {
        $this->lessonsAssigned[] = $lessonsAssigned;

        return $this;
    }

    /**
     * Remove lessonsAssigned
     *
     * @param \Metaflo\SchemoBundle\Entity\Lesson $lessonsAssigned
     */
    public function removeLessonsAssigned(\Metaflo\SchemoBundle\Entity\Lesson $lessonsAssigned) {
        $this->lessonsAssigned->removeElement($lessonsAssigned);
    }

    /**
     * Get lessonsAssigned
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLessonsAssigned() {
        return $this->lessonsAssigned;
    }

    /**
     * Add lessonsPossible
     *
     * @param \Metaflo\SchemoBundle\Entity\LessonTemplate $lessonsPossible
     * @return Teacher
     */
    public function addLessonsPossible(\Metaflo\SchemoBundle\Entity\LessonTemplate $lessonsPossible) {
        $this->lessonsPossible[] = $lessonsPossible;

        return $this;
    }

    /**
     * Remove lessonsPossible
     *
     * @param \Metaflo\SchemoBundle\Entity\LessonTemplate $lessonsPossible
     */
    public function removeLessonsPossible(\Metaflo\SchemoBundle\Entity\LessonTemplate $lessonsPossible) {
        $this->lessonsPossible->removeElement($lessonsPossible);
    }

    /**
     * Get lessonsPossible
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLessonsPossible() {
        return $this->lessonsPossible;
    }

    /**
     * Set project
     *
     * @param \Metaflo\SchemoBundle\Entity\Project $project
     * @return Teacher
     */
    public function setProject(\Metaflo\SchemoBundle\Entity\Project $project = null) {
        $this->project = $project;

        return $this;
    }

    /**
     * Get project
     *
     * @return \Metaflo\SchemoBundle\Entity\Project
     */
    public function getProject() {
        return $this->project;
    }

    function __toString() {
        return $this->name;
    }
}
