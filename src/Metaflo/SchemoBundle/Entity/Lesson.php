<?php

namespace Metaflo\SchemoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\MaxDepth;

/**
 * Lesson
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ExclusionPolicy("all")
 *
 */
class Lesson {
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Room
     *
     * @Expose
     * @MaxDepth(1)
     * @ORM\ManyToOne(targetEntity="Room", inversedBy="lessonsAssigned")
     **/
    private $room;

    /**
     * @var Teacher
     *
     * @ORM\ManyToOne(targetEntity="Teacher", inversedBy="lessonsAssigned")
     * @Expose
     **/
    private $teacher;

    /**
     * @var Timeslot
     *
     * @ORM\ManyToOne(targetEntity="Timeslot", inversedBy="lessons")
     * @Expose
     */
    private $timeslot;

    /**
     * @var integer
     * @Expose
     * @ORM\Column(type="integer")
     */
    private $weekNo;

    /**
     * @var integer
     * @Expose
     * @ORM\Column(type="integer")
     */
    private $year;

    /**
     * @var boolean
     *
     * @Expose
     * @ORM\Column(name="cancelled", type="boolean")
     */
    private $cancelled = false;

    /**
     * @var LessonTemplate
     *
     * @ORM\ManyToOne(targetEntity="LessonTemplate", inversedBy="lessonsDerived")
     * @Expose
     */
    private $template;

    /**
     * @var Project
     *
     * @ORM\ManyToOne(targetEntity="Project", inversedBy="lessons")
     */
    private $project;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set room
     *
     * @param string $room
     * @return Lesson
     */
    public function setRoom($room) {
        $this->room = $room;

        return $this;
    }

    /**
     * Get room
     *
     * @return Room
     */
    public function getRoom() {
        return $this->room;
    }

    /**
     * Set teacher
     *
     * @param string $teacher
     * @return Lesson
     */
    public function setTeacher($teacher) {
        $this->teacher = $teacher;

        return $this;
    }

    /**
     * Get teacher
     *
     * @return Teacher
     */
    public function getTeacher() {
        return $this->teacher;
    }

    /**
     * Set timeslot
     *
     * @param string $timeslot
     * @return Lesson
     */
    public function setTimeslot($timeslot) {
        $this->timeslot = $timeslot;

        return $this;
    }

    /**
     * Get timeslot
     *
     * @return Timeslot
     */
    public function getTimeslot() {
        return $this->timeslot;
    }

    /**
     * Set cancelled
     *
     * @param boolean $cancelled
     * @return Lesson
     */
    public function setCancelled($cancelled) {
        $this->cancelled = $cancelled;

        return $this;
    }

    /**
     * Get cancelled
     *
     * @return boolean
     */
    public function getCancelled() {
        return $this->cancelled;
    }

    /**
     * Set template
     *
     * @param string $template
     * @return Lesson
     */
    public function setTemplate($template) {
        $this->template = $template;

        return $this;
    }

    /**
     * Get template
     *
     * @return LessonTemplate
     */
    public function getTemplate() {
        return $this->template;
    }

    /**
     * Set project
     *
     * @param \Metaflo\SchemoBundle\Entity\Project $project
     * @return Lesson
     */
    public function setProject(\Metaflo\SchemoBundle\Entity\Project $project = null)
    {
        $this->project = $project;

        return $this;
    }

    /**
     * Get project
     *
     * @return \Metaflo\SchemoBundle\Entity\Project 
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * Set weekNo
     *
     * @param integer $weekNo
     * @return Lesson
     */
    public function setWeekNo($weekNo)
    {
        $this->weekNo = $weekNo;

        return $this;
    }

    /**
     * Get weekNo
     *
     * @return integer 
     */
    public function getWeekNo()
    {
        return $this->weekNo;
    }

    /**
     * Set year
     *
     * @param integer $year
     * @return Lesson
     */
    public function setYear($year)
    {
        $this->year = $year;

        return $this;
    }

    /**
     * Get year
     *
     * @return integer 
     */
    public function getYear()
    {
        return $this->year;
    }
}
