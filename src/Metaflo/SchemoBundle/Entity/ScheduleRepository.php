<?php
/**
 * Created by PhpStorm.
 * User: gust
 * Date: 30.01.14
 * Time: 14:10
 */

namespace Metaflo\SchemoBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Metaflo\SchemoBundle\Entity\Schedule;

class ScheduleRepository extends EntityRepository {
    public function getCalData($projectID, $schoolgroupID, $start, $end) {
        $startDatetime = new \DateTime($end);
        $week = $startDatetime->format("W");
        $year = $startDatetime->format("Y");

        /** @var EntityManager $em */
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
                    ->select("l")->from("MetafloSchemoBundle:Lesson", 'l')
                    ->leftJoin("l.project", 'p')
                    ->leftJoin("l.timeslot", 'ts')
                    ->leftJoin("l.template", 'tm')
                    ->leftJoin("tm.schoolgroup", 'sg')

                    ->where('p.id = :pid')
                    ->andWhere('sg.id = :sgid')
                    ->andWhere("l.weekNo = :week")
                    ->andWhere("l.year = :year")

                    ->setParameter("pid", (int) $projectID)
                    ->setParameter("sgid", (int) $schoolgroupID)
                    ->setParameter("week", $week)
                    ->setParameter("year", $year)
                    ->getQuery();

        return $query->getResult();

    }

    /**
     * @param integer $project
     * @param integer $teacher
     * @param integer $week
     * @param integer $timeslot
     * @return ArrayCollection|Lesson[]
     */
    public function getLesson($project, $teacher, $week, $timeslot) {
        /** @var EntityManager $em */
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
                    ->select("l")->from("MetafloSchemoBundle:Lesson", 'l')
                    ->leftJoin("l.project", 'p')
                    ->leftJoin("l.timeslot", 'ts')
                    ->leftJoin("l.teacher", 'te')

                    ->where('p.id = :pid')
                    ->andWhere("l.weekNo = :week")
                    ->andWhere("ts.id = :tsid")
                    ->andWhere("te.id = :teid")
                    ->andWhere("l.weekNo = :week")
                    ->andWhere("l.year = :year")

                    ->setParameter("pid", (int) $project)
                    ->setParameter("tsid", (int) $timeslot)
                    ->setParameter("teid", (int) $teacher)
                    ->setParameter("week", (int) $week)
                    ->setParameter("year", (int) date("Y")) //TODO: quick and very, very dirty... do better!
                    ->getQuery();
        return $query->getResult();
    }

    public function getCancelledLessons($project) {
        $startDatetime = new \DateTime($end);
        $week = $startDatetime->format("W");
        $year = $startDatetime->format("Y");

        /** @var EntityManager $em */
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
                    ->select("l")->from("MetafloSchemoBundle:Lesson", 'l')
                    ->leftJoin("l.project", 'p')
                    ->leftJoin("l.timeslot", 'ts')
                    ->leftJoin("l.template", 'tm')
                    ->leftJoin("tm.schoolgroup", 'sg')

                    ->where('p.id = :pid')
                    ->andWhere('sg.id = :sgid')
                    ->andWhere("l.weekNo = :week")
                    ->andWhere("l.year = :year")

                    ->setParameter("pid", (int) $projectID)
                    ->setParameter("sgid", (int) $schoolgroupID)
                    ->setParameter("week", $week)
                    ->setParameter("year", $year)
                    ->getQuery();

        return $query->getResult();

    }
}