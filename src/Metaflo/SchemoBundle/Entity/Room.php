<?php

namespace Metaflo\SchemoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\MaxDepth;

/**
 * Room
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ExclusionPolicy("all")
 */
class Room {
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="usageType", type="smallint")
     */
    private $usageType;

    const USAGE_CLASS = 0;
    const USAGE_ACTIVITY = 1;
    public static $usages
        = array(
            self::USAGE_CLASS => "class",
            self::USAGE_ACTIVITY => "activity"
        );

    /**
     * @var string
     *
     * @ORM\Column(name="usageName", type="string", length=255)
     */
    private $usageName;

    /**
     * @var string
     *
     * @Expose
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var LessonTemplate[]
     *
     * @ORM\OneToMany(targetEntity="LessonTemplate", mappedBy="roomTypical")
     */
    private $lessonTemplates;

    /**
     * @var Lesson[]
     *
     * @ORM\OneToMany(targetEntity="Lesson", mappedBy="room")
     */
    private $lessonsAssigned;

    /**
     * @var Project
     *
     * @ORM\ManyToOne(targetEntity="Project", inversedBy="rooms")
     */
    private $project;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set usageType
     *
     * @param integer $usageType
     * @return Room
     */
    public function setUsageType($usageType) {
        if (array_key_exists($usageType, self::$usages)) {
            $this->usageType = $usageType;
        }

        return $this;
    }

    /**
     * Get usageType
     *
     * @return integer
     */
    public function getUsageType() {
        return $this->usageType;
    }

    /**
     * Set usageName
     *
     * @param string $usageName
     * @return Room
     */
    public function setUsageName($usageName) {
        $this->usageName = $usageName;

        return $this;
    }

    /**
     * Get usageName
     *
     * @return string
     */
    public function getUsageName() {
        return $this->usageName;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Room
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Constructor
     */
    public function __construct() {
        $this->lessonTemplates = new \Doctrine\Common\Collections\ArrayCollection();
        $this->lessonsAssigned = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add lessonTemplates
     *
     * @param \Metaflo\SchemoBundle\Entity\LessonTemplate $lessonTemplates
     * @return Room
     */
    public function addLessonTemplate(\Metaflo\SchemoBundle\Entity\LessonTemplate $lessonTemplates) {
        $this->lessonTemplates[] = $lessonTemplates;

        return $this;
    }

    /**
     * Remove lessonTemplates
     *
     * @param \Metaflo\SchemoBundle\Entity\LessonTemplate $lessonTemplates
     */
    public function removeLessonTemplate(\Metaflo\SchemoBundle\Entity\LessonTemplate $lessonTemplates) {
        $this->lessonTemplates->removeElement($lessonTemplates);
    }

    /**
     * Get lessonTemplates
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLessonTemplates() {
        return $this->lessonTemplates;
    }

    /**
     * Add lessonsAssigned
     *
     * @param \Metaflo\SchemoBundle\Entity\Lesson $lessonsAssigned
     * @return Room
     */
    public function addLessonsAssigned(\Metaflo\SchemoBundle\Entity\Lesson $lessonsAssigned) {
        $this->lessonsAssigned[] = $lessonsAssigned;

        return $this;
    }

    /**
     * Remove lessonsAssigned
     *
     * @param \Metaflo\SchemoBundle\Entity\Lesson $lessonsAssigned
     */
    public function removeLessonsAssigned(\Metaflo\SchemoBundle\Entity\Lesson $lessonsAssigned) {
        $this->lessonsAssigned->removeElement($lessonsAssigned);
    }

    /**
     * Get lessonsAssigned
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLessonsAssigned() {
        return $this->lessonsAssigned;
    }

    /**
     * Set project
     *
     * @param \Metaflo\SchemoBundle\Entity\Project $project
     * @return Room
     */
    public function setProject(\Metaflo\SchemoBundle\Entity\Project $project = null) {
        $this->project = $project;

        return $this;
    }

    /**
     * Get project
     *
     * @return \Metaflo\SchemoBundle\Entity\Project
     */
    public function getProject() {
        return $this->project;
    }

    function __toString() {
        return $this->name;
    }

    public function getUsageTypeName() {
        return self::$usages[$this->usageType];
    }
}
