<?php

namespace Metaflo\SchemoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\MaxDepth;

/**
 * LessonTemplate
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ExclusionPolicy("all")
 */
class LessonTemplate {
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Expose
     */
    private $name;

    /**
     * @var Room
     *
     * @ORM\ManyToOne(targetEntity="Room", inversedBy="lessonTemplates")
     */
    private $roomTypical;

    /**
     * @var Teacher[]
     *
     * @ORM\ManyToMany(targetEntity="Teacher", inversedBy="lessonsPossible")
     * @ORM\JoinTable(name="teacher_lessons_possible")
     */
    private $teachers;

    /**
     * @var Schoolgroup
     *
     * @ORM\ManyToOne(targetEntity="Schoolgroup", inversedBy="lessonTemplates")
     * @Expose
     */
    private $schoolgroup;

    /**
     * @var Lesson
     *
     * @ORM\OneToMany(targetEntity="Lesson", mappedBy="template")
     */
    private $lessonsDerived;

    /**
     * @var Project
     *
     * @ORM\ManyToOne(targetEntity="Project", inversedBy="lessonTemplates")
     */
    private $project;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set roomTypical
     *
     * @param Room $roomTypical
     * @return LessonTemplate
     */
    public function setRoomTypical(Room $roomTypical) {
        $this->roomTypical = $roomTypical;

        return $this;
    }

    /**
     * Get roomTypical
     *
     * @return Room
     */
    public function getRoomTypical() {
        return $this->roomTypical;
    }

    /**
     * Set teachers
     *
     * @param Teacher[] $teachers
     * @return LessonTemplate
     */
    public function setTeachers($teachers) {
        $this->teachers = $teachers;

        return $this;
    }

    /**
     * Get teachers
     *
     * @return Teacher[]
     */
    public function getTeachers() {
        return $this->teachers;
    }

    /**
     * Set schoolgroup
     *
     * @param Schoolgroup $schoolgroup
     * @return LessonTemplate
     */
    public function setSchoolgroup($schoolgroup) {
        $this->schoolgroup = $schoolgroup;

        return $this;
    }

    /**
     * Get schoolgroup
     *
     * @return Schoolgroup
     */
    public function getSchoolgroup() {
        return $this->schoolgroup;
    }

    /**
     * Set requiredLessons
     *
     * @param integer $requiredLessons
     * @return LessonTemplate
     */
    public function setRequiredLessons($requiredLessons) {
        $this->requiredLessons = $requiredLessons;

        return $this;
    }

    /**
     * Get requiredLessons
     *
     * @return integer
     */
    public function getRequiredLessons() {
        return $this->requiredLessons;
    }

    /**
     * Constructor
     */
    public function __construct() {
        $this->teachers = new \Doctrine\Common\Collections\ArrayCollection();
        $this->lessonsDerived = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add teachers
     *
     * @param \Metaflo\SchemoBundle\Entity\Teacher $teachers
     * @return LessonTemplate
     */
    public function addTeacher(\Metaflo\SchemoBundle\Entity\Teacher $teachers) {
        $this->teachers[] = $teachers;

        return $this;
    }

    /**
     * Remove teachers
     *
     * @param \Metaflo\SchemoBundle\Entity\Teacher $teachers
     */
    public function removeTeacher(\Metaflo\SchemoBundle\Entity\Teacher $teachers) {
        $this->teachers->removeElement($teachers);
    }

    /**
     * Add lessonsDerived
     *
     * @param \Metaflo\SchemoBundle\Entity\Lesson $lessonsDerived
     * @return LessonTemplate
     */
    public function addLessonsDerived(\Metaflo\SchemoBundle\Entity\Lesson $lessonsDerived) {
        $this->lessonsDerived[] = $lessonsDerived;

        return $this;
    }

    /**
     * Remove lessonsDerived
     *
     * @param \Metaflo\SchemoBundle\Entity\Lesson $lessonsDerived
     */
    public function removeLessonsDerived(\Metaflo\SchemoBundle\Entity\Lesson $lessonsDerived) {
        $this->lessonsDerived->removeElement($lessonsDerived);
    }

    /**
     * Get lessonsDerived
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLessonsDerived() {
        return $this->lessonsDerived;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return LessonTemplate
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set project
     *
     * @param \Metaflo\SchemoBundle\Entity\Project $project
     * @return LessonTemplate
     */
    public function setProject(\Metaflo\SchemoBundle\Entity\Project $project = null) {
        $this->project = $project;

        return $this;
    }

    /**
     * Get project
     *
     * @return \Metaflo\SchemoBundle\Entity\Project
     */
    public function getProject() {
        return $this->project;
    }

    function __toString() {
        return $this->name . " (".
        $this->schoolgroup->getName().", ".
        $this->roomTypical->getName().", ".
        implode("/",$this->teachers->toArray()).")";
    }
}
