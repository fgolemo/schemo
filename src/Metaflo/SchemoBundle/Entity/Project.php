<?php

namespace Metaflo\SchemoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Project
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Project {
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var Lesson[]
     *
     * @ORM\OneToMany(targetEntity="Lesson", mappedBy="project")
     */
    private $lessons;

    /**
     * @var LessonTemplate[]
     *
     * @ORM\OneToMany(targetEntity="LessonTemplate", mappedBy="project")
     */
    private $lessonTemplates;

    /**
     * @var Room[]
     *
     * @ORM\OneToMany(targetEntity="Room", mappedBy="project")
     */
    private $rooms;

    /**
     * @var Schoolgroup[]
     *
     * @ORM\OneToMany(targetEntity="Schoolgroup", mappedBy="project")
     */
    private $schoolgroups;

    /**
     * @var Teacher[]
     *
     * @ORM\OneToMany(targetEntity="Teacher", mappedBy="project")
     */
    private $teachers;

    /**
     * @var Timeslot[]
     *
     * @ORM\OneToMany(targetEntity="Timeslot", mappedBy="project")
     */
    private $timeslots;

    /**
     * @var Schedule;
     *
     * @ORM\OneToOne(targetEntity="Schedule", mappedBy="project")
     */
    private $schedule;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Project
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Constructor
     */
    public function __construct() {
        $this->lessons = new \Doctrine\Common\Collections\ArrayCollection();
        $this->lessonTemplates = new \Doctrine\Common\Collections\ArrayCollection();
        $this->rooms = new \Doctrine\Common\Collections\ArrayCollection();
        $this->schoolgroups = new \Doctrine\Common\Collections\ArrayCollection();
        $this->teachers = new \Doctrine\Common\Collections\ArrayCollection();
        $this->timeslots = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add lessons
     *
     * @param \Metaflo\SchemoBundle\Entity\Lesson $lessons
     * @return Project
     */
    public function addLesson(\Metaflo\SchemoBundle\Entity\Lesson $lessons) {
        $this->lessons[] = $lessons;

        return $this;
    }

    /**
     * Remove lessons
     *
     * @param \Metaflo\SchemoBundle\Entity\Lesson $lessons
     */
    public function removeLesson(\Metaflo\SchemoBundle\Entity\Lesson $lessons) {
        $this->lessons->removeElement($lessons);
    }

    /**
     * Get lessons
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLessons() {
        return $this->lessons;
    }

    /**
     * Add lessonTemplates
     *
     * @param \Metaflo\SchemoBundle\Entity\LessonTemplate $lessonTemplates
     * @return Project
     */
    public function addLessonTemplate(\Metaflo\SchemoBundle\Entity\LessonTemplate $lessonTemplates) {
        $this->lessonTemplates[] = $lessonTemplates;

        return $this;
    }

    /**
     * Remove lessonTemplates
     *
     * @param \Metaflo\SchemoBundle\Entity\LessonTemplate $lessonTemplates
     */
    public function removeLessonTemplate(\Metaflo\SchemoBundle\Entity\LessonTemplate $lessonTemplates) {
        $this->lessonTemplates->removeElement($lessonTemplates);
    }

    /**
     * Get lessonTemplates
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLessonTemplates() {
        return $this->lessonTemplates;
    }

    /**
     * Add rooms
     *
     * @param \Metaflo\SchemoBundle\Entity\Room $rooms
     * @return Project
     */
    public function addRoom(\Metaflo\SchemoBundle\Entity\Room $rooms) {
        $this->rooms[] = $rooms;

        return $this;
    }

    /**
     * Remove rooms
     *
     * @param \Metaflo\SchemoBundle\Entity\Room $rooms
     */
    public function removeRoom(\Metaflo\SchemoBundle\Entity\Room $rooms) {
        $this->rooms->removeElement($rooms);
    }

    /**
     * Get rooms
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRooms() {
        return $this->rooms;
    }

    /**
     * Add schoolgroups
     *
     * @param \Metaflo\SchemoBundle\Entity\Schoolgroup $schoolgroups
     * @return Project
     */
    public function addSchoolgroup(\Metaflo\SchemoBundle\Entity\Schoolgroup $schoolgroups) {
        $this->schoolgroups[] = $schoolgroups;

        return $this;
    }

    /**
     * Remove schoolgroups
     *
     * @param \Metaflo\SchemoBundle\Entity\Schoolgroup $schoolgroups
     */
    public function removeSchoolgroup(\Metaflo\SchemoBundle\Entity\Schoolgroup $schoolgroups) {
        $this->schoolgroups->removeElement($schoolgroups);
    }

    /**
     * Get schoolgroups
     *
     * @return Schoolgroup[]
     */
    public function getSchoolgroups() {
        return $this->schoolgroups;
    }

    /**
     * Add teachers
     *
     * @param \Metaflo\SchemoBundle\Entity\Teacher $teachers
     * @return Project
     */
    public function addTeacher(\Metaflo\SchemoBundle\Entity\Teacher $teachers) {
        $this->teachers[] = $teachers;

        return $this;
    }

    /**
     * Remove teachers
     *
     * @param \Metaflo\SchemoBundle\Entity\Teacher $teachers
     */
    public function removeTeacher(\Metaflo\SchemoBundle\Entity\Teacher $teachers) {
        $this->teachers->removeElement($teachers);
    }

    /**
     * Get teachers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTeachers() {
        return $this->teachers;
    }

    /**
     * Add timeslots
     *
     * @param \Metaflo\SchemoBundle\Entity\Timeslot $timeslots
     * @return Project
     */
    public function addTimeslot(\Metaflo\SchemoBundle\Entity\Timeslot $timeslots) {
        $this->timeslots[] = $timeslots;

        return $this;
    }

    /**
     * Remove timeslots
     *
     * @param \Metaflo\SchemoBundle\Entity\Timeslot $timeslots
     */
    public function removeTimeslot(\Metaflo\SchemoBundle\Entity\Timeslot $timeslots) {
        $this->timeslots->removeElement($timeslots);
    }

    /**
     * Get timeslots
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTimeslots() {
        return $this->timeslots;
    }

    function __toString() {
        return $this->name;
    }


    /**
     * Set schedule
     *
     * @param \Metaflo\SchemoBundle\Entity\Schedule $schedule
     * @return Project
     */
    public function setSchedule(\Metaflo\SchemoBundle\Entity\Schedule $schedule = null) {
        $this->schedule = $schedule;

        return $this;
    }

    /**
     * Get schedule
     *
     * @return \Metaflo\SchemoBundle\Entity\Schedule
     */
    public function getSchedule() {
        return $this->schedule;
    }
}
