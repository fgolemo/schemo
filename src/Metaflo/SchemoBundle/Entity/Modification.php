<?php
/**
 * Created by PhpStorm.
 * User: gust
 * Date: 02.02.14
 * Time: 01:30
 */

namespace Metaflo\SchemoBundle\Entity;


class Modification {

    /**
     * @var Teacher
     */
    private $teacher;

    /**
     * @var Timeslot
     */
    private $timeslot;

    /**
     * @var integer
     */
    private $week;

    /**
     * @var Project
     */
    private $project;

    function __construct() {
        $this->date = new \DateTime("now");
    }


    /**
     * @param integer $week
     * @return $this
     */
    public function setWeek($week) {
        $this->week = $week;

        return $this;
    }

    /**
     * @return integer
     */
    public function getWeek() {
        return $this->week;
    }

    /**
     * @param \Metaflo\SchemoBundle\Entity\Teacher $teacher
     * @return $this
     */
    public function setTeacher($teacher) {
        $this->teacher = $teacher;

        return $this;
    }

    /**
     * @return \Metaflo\SchemoBundle\Entity\Teacher
     */
    public function getTeacher() {
        return $this->teacher;
    }

    /**
     * @param \Metaflo\SchemoBundle\Entity\Project $project
     * @return $this
     */
    public function setProject($project) {
        $this->project = $project;

        return $this;
    }

    /**
     * @return \Metaflo\SchemoBundle\Entity\Project
     */
    public function getProject() {
        return $this->project;
    }

    /**
     * @param \Metaflo\SchemoBundle\Entity\Timeslot $timeslot
     * @return $this
     */
    public function setTimeslot($timeslot) {
        $this->timeslot = $timeslot;
        return $this;
    }

    /**
     * @return \Metaflo\SchemoBundle\Entity\Timeslot
     */
    public function getTimeslot() {
        return $this->timeslot;
    }


} 