<?php

namespace Metaflo\SchemoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Schedule
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="Metaflo\SchemoBundle\Entity\ScheduleRepository")
 */
class Schedule {
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="smallint")
     */
    private $status = 0;

    const STATUS_NOTDONE = 0;
    const STATUS_COMPLETED_ERRONOUS = 1;
    const STATUS_COMPLETED_GOOD = 2;
    public static $statuses = [
        self::STATUS_NOTDONE => "not done yet",
        self::STATUS_COMPLETED_ERRONOUS => "completed, with errors",
        self::STATUS_COMPLETED_GOOD => "completed"
    ];

    /**
     * @var integer
     *
     * @ORM\Column(name="collisions", type="integer")
     */
    private $collisions = 0;

    /**
     * @var float
     *
     * @ORM\Column(name="quality", type="float")
     */
    private $quality = 0;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datetime", type="datetime", nullable=true)
     */
    private $datetime;

    /**
     * @var Project
     *
     * @ORM\OneToOne(targetEntity="Project", inversedBy="schedule")
     */
    private $project;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateStart;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateEnd;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Schedule
     */
    public function setStatus($status) {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus() {
        return $this->status;
    }

    /**
     * Set collisions
     *
     * @param integer $collisions
     * @return Schedule
     */
    public function setCollisions($collisions) {
        $this->collisions = $collisions;

        return $this;
    }

    /**
     * Get collisions
     *
     * @return integer
     */
    public function getCollisions() {
        return $this->collisions;
    }

    /**
     * Set quality
     *
     * @param float $quality
     * @return Schedule
     */
    public function setQuality($quality) {
        $this->quality = $quality;

        return $this;
    }

    /**
     * Get quality
     *
     * @return float
     */
    public function getQuality() {
        return $this->quality;
    }

    /**
     * Set datetime
     *
     * @param \DateTime $datetime
     * @return Schedule
     */
    public function setDatetime($datetime) {
        $this->datetime = $datetime;

        return $this;
    }

    /**
     * Get datetime
     *
     * @return \DateTime
     */
    public function getDatetime() {
        return $this->datetime;
    }

    /**
     * Set project
     *
     * @param \Metaflo\SchemoBundle\Entity\Project $project
     * @return Schedule
     */
    public function setProject(\Metaflo\SchemoBundle\Entity\Project $project = null)
    {
        $this->project = $project;

        return $this;
    }

    /**
     * Get project
     *
     * @return \Metaflo\SchemoBundle\Entity\Project 
     */
    public function getProject()
    {
        return $this->project;
    }

    public function getStatusName() {
        return self::$statuses[$this->status];
    }

    /**
     * Set dateStart
     *
     * @param \DateTime $dateStart
     * @return Schedule
     */
    public function setDateStart($dateStart)
    {
        $this->dateStart = $dateStart;

        return $this;
    }

    /**
     * Get dateStart
     *
     * @return \DateTime 
     */
    public function getDateStart()
    {
        return $this->dateStart;
    }

    /**
     * Set dateEnd
     *
     * @param \DateTime $dateEnd
     * @return Schedule
     */
    public function setDateEnd($dateEnd)
    {
        $this->dateEnd = $dateEnd;

        return $this;
    }

    /**
     * Get dateEnd
     *
     * @return \DateTime 
     */
    public function getDateEnd()
    {
        return $this->dateEnd;
    }
}
