<?php
/**
 * Created by PhpStorm.
 * User: gust
 * Date: 02.02.14
 * Time: 00:10
 */

namespace Metaflo\SchemoBundle\Reasoner;


use Doctrine\ORM\EntityManager;
use Metaflo\SchemoBundle\Entity\Lesson;
use Metaflo\SchemoBundle\Entity\Project;
use Metaflo\SchemoBundle\Entity\Teacher;
use Metaflo\SchemoBundle\Entity\Timeslot;
use Symfony\Component\Routing\Exception\InvalidParameterException;

class Checker {
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var array
     */
    private $config;

    /**
     * @var array
     */
    private $endpoints;

    /**
     * @var Lesson
     */
    private $lesson;

    const LESSON_CANCEL_PERCENT_TEMPLATE = 25; //%
    const LESSON_CANCEL_PERCENT_CURRICULUM = 25; //%

    function __construct(EntityManager $entityManager, $config, $endpoints, $lesson) {
        $this->em = $entityManager;
        $this->config = $config;
        $this->endpoints = $endpoints;
        $this->lesson = $this->em->getRepository('MetafloSchemoBundle:Lesson')->find($lesson);
        if (!$this->lesson) {
            throw new InvalidParameterException("Lesson $lesson couldn't be found");
        }
    }

    public function safeCancelCheck() {
        return [
            "feedbackFunction" => "safeCancelFeedback",
            "input" => [
                "type" => "radio",
                "text" => "Is this class close to the exams and the material relevant?",
                "options" => ["no", "yes"],
            ],
        ];
    }

    public function safeCancelFeedback($answer = null) {
        if ($answer == true) { // true: exams are close and mat is relevant
            return [
                "answer" => false,
                "reason" => "exams are too close / mat is relevant"
            ];
        }
        // else make checks

        // get number of lessons cancelled in this lesson series
        $cancelledLessonsTpl = $this->getLessonsBy("template", true);
        // get number of lessons cancelled in the whole curriculum
        $cancelledLessonsCur = $this->getLessonsBy("curriculum", true);
        // if either number is above the corresp. threshold, it's not safe to cancel, answer false
        $totalLessonsTpl = $this->getLessonsBy("template", false);
        $totalLessonsCur = $this->getLessonsBy("curriculum", false);
        $percentTplCancelled = 100 * $cancelledLessonsTpl / $totalLessonsTpl;
        $percentCurCancelled = 100 * $cancelledLessonsCur / $totalLessonsCur;

        if ($percentTplCancelled >= self::LESSON_CANCEL_PERCENT_TEMPLATE ||
            $percentCurCancelled >= self::LESSON_CANCEL_PERCENT_CURRICULUM
        ) {
            return [
                "answer" => false,
                "reason" => "$percentTplCancelled % courses cancelled in this course,
                $percentCurCancelled % in the whole curriculum"
            ]; //true = can be cancelled, false = should not be cancelled
        } else {
            return [
                "answer" => true,
                "reason" => "not many cancellations either in the course or in the whole curriculum
                ($percentTplCancelled% and $percentCurCancelled%)"
            ];
        }
    }

    public function teachersFreeCheck() {
        return [
            "feedbackFunction" => "teachersFreeFeedback",
        ];
    }

    public function teachersFreeFeedback($answer = null) {
        $totalTeachers = count($this->getTotalTeachers());
        $occTeachers = count($this->getOccupiedTeachers());

        if ($occTeachers == $totalTeachers) {
            return [
                "answer" => false,
                "reason" => "found $totalTeachers teachers, but all are occupied"
            ]; //nothing free
        } else {
            return [
                "answer" => true,
                "reason" => "at least ".($totalTeachers*1-$occTeachers*1)." teachers appear(s) to be free"
            ];
        }
    }

    public function teachersQualifiedCheck() {
        return [
            "feedbackFunction" => "teachersQualifiedFeedback",
        ];
    }

    public function teachersQualifiedFeedback($answer = null) {
        $teachers = $this->lesson->getTemplate()->getTeachers();
        $timeslotLessons = $this->getAllLessonsInTimeslot();
        foreach ($teachers as $teacher) {
            // is teacher free?
            $free = true;
            foreach ($timeslotLessons as $lesson) {
                if ($lesson->getTeacher() == $teacher) {
                    $free = false;
                }
            }
            if ($free) // teacher is in no course right now, so we found one and can exit
                return [
                    "answer" => true,
                    "reason" => "teacher ".$teacher->getName()." appears to be free and can teach the course"
            ];
        }
        return [
            "answer" => false,
            "reason" => "the course can be taught by ".count($teachers)." teacher(s), but none of them is free"
        ]; // didnt find anybody among the available ones
    }

    public function originalMaterialCheck() {
        return [
            "feedbackFunction" => "originalMaterialFeedback",
            "input" => [
                "type" => "radio",
                "text" => "Can the original teacher provide some worksheets or any material to give into the class?",
                "options" => ["no", "yes"],
            ],
        ];
    }

    public function originalMaterialFeedback($answer = null) {
        return [
            "answer" => $answer,
            "reason" => "because you responded with ".($answer?"Yes":"No")
        ];
    }

    public function otherCourseCheck() {
        return [
            "feedbackFunction" => "otherCourseFeedback",
        ];
    }

    public function otherCourseFeedback($answer = null) {
        //TODO: implement
    }

    /**
     * @param string $by
     * @param bool $onlyCancelled
     * @return integer
     */
    private function getLessonsBy($by = "template", $onlyCancelled = false) {
        $query = $this->em->createQueryBuilder()
                          ->select("count(l) as lessonCount")->from("MetafloSchemoBundle:Lesson", 'l')
                          ->leftJoin("l.template", 'tm');


        if ($by == "template") {
            $query->where('tm.id = :tmid')
                  ->setParameter("tmid", (int) $this->lesson->getTemplate()->getId());
        } else { // by = curriculum, i.e. by class
            $query->leftJoin("tm.schoolgroup", 'sg')
                  ->where('sg.id = :sgid')
                  ->setParameter("sgid", (int) $this->lesson->getTemplate()->getSchoolgroup()->getId());
        }

        if ($onlyCancelled) {
            $query->andWhere('l.cancelled = 1');
        }

        return $query->getQuery()->getSingleScalarResult();
    }

    /**
     * @return Lesson[]
     */
    private function getOccupiedTeachers() {
        $query = $this->em->createQueryBuilder()
                          ->select("l")->from("MetafloSchemoBundle:Lesson", 'l')
                          ->innerJoin("l.teacher", "te")
                          ->innerJoin("l.timeslot", "ts")
                          ->where("ts.id = :tsid")
                          ->andWhere("l.weekNo = :week")
                          ->andWhere("l.year = :year")
                          ->andWhere("l.cancelled = false")
                          ->setParameter("tsid", (int) $this->lesson->getTimeslot()->getId())
                          ->setParameter("week", (int) $this->lesson->getWeekNo())
                          ->setParameter("year", (int) $this->lesson->getYear())
                          ->getQuery();

        return $query->getResult();
    }

    /**
     * @return Teacher[]
     */
    private function getTotalTeachers() {
        $query = $this->em->createQueryBuilder()
                          ->select("te")->from("MetafloSchemoBundle:Teacher", 'te')
                          ->leftJoin("te.project", 'p')
                          ->where("p.id = :pid")
                          ->setParameter("pid", (int) $this->lesson->getProject()->getId());

        return $query->getQuery()->getResult();

    }

    // does not work... or took too much time
//    private function getFreeTeachers() {
//        $query = $this->em->createQuery(
//                          "
//              SELECT te
//              FROM MetafloSchemoBundle:Teacher te
//              LEFT OUTER JOIN MetafloSchemoBundle:Lesson l WITH l.teacher = te
//              INNER JOIN l.timeslot ts
//              WHERE l.weekNo = :week
//              AND l.year = :year
//              AND l.cancelled = 0
//              AND ts.id = :tsid
//              AND te.id is null
//                                  "
//        )
//                          ->setParameter("tsid", (int) $this->lesson->getTimeslot()->getId())
//                          ->setParameter("week", (int) $this->lesson->getWeekNo())
//                          ->setParameter("year", (int) $this->lesson->getYear());
//
//        return $query->getResult();
//    }

    /**
     * @return Lesson[]
     */
    private function getAllLessonsInTimeslot() {
        $query = $this->em->createQueryBuilder()
                          ->select("l")->from("MetafloSchemoBundle:Lesson", 'l')
                          ->leftJoin("l.timeslot", 'ts')
                          ->leftJoin("l.teacher", 'te')
                          ->where("ts.id = :tsid")
                          ->andWhere("l.weekNo = :week")
                          ->andWhere("l.year = :year")
                          ->andWhere("l.cancelled = false")
                          ->setParameter("tsid", (int) $this->lesson->getTimeslot()->getId())
                          ->setParameter("week", (int) $this->lesson->getWeekNo())
                          ->setParameter("year", (int) $this->lesson->getYear())
                          ->getQuery();

        return $query->getResult();
    }
}