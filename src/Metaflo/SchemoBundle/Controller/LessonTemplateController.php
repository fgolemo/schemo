<?php

namespace Metaflo\SchemoBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Metaflo\SchemoBundle\Entity\LessonTemplate;
use Metaflo\SchemoBundle\Form\LessonTemplateType;

/**
 * LessonTemplate controller.
 *
 * @Route("/lessontemplate")
 */
class LessonTemplateController extends Controller
{

    /**
     * Lists all LessonTemplate entities.
     *
     * @Route("/", name="lessontemplate")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('MetafloSchemoBundle:LessonTemplate')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new LessonTemplate entity.
     *
     * @Route("/", name="lessontemplate_create")
     * @Method("POST")
     * @Template("MetafloSchemoBundle:LessonTemplate:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new LessonTemplate();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('lessontemplate_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a LessonTemplate entity.
    *
    * @param LessonTemplate $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(LessonTemplate $entity)
    {
        $form = $this->createForm(new LessonTemplateType(), $entity, array(
            'action' => $this->generateUrl('lessontemplate_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new LessonTemplate entity.
     *
     * @Route("/new", name="lessontemplate_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new LessonTemplate();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a LessonTemplate entity.
     *
     * @Route("/{id}", name="lessontemplate_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MetafloSchemoBundle:LessonTemplate')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find LessonTemplate entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing LessonTemplate entity.
     *
     * @Route("/{id}/edit", name="lessontemplate_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MetafloSchemoBundle:LessonTemplate')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find LessonTemplate entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a LessonTemplate entity.
    *
    * @param LessonTemplate $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(LessonTemplate $entity)
    {
        $form = $this->createForm(new LessonTemplateType(), $entity, array(
            'action' => $this->generateUrl('lessontemplate_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing LessonTemplate entity.
     *
     * @Route("/{id}", name="lessontemplate_update")
     * @Method("PUT")
     * @Template("MetafloSchemoBundle:LessonTemplate:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MetafloSchemoBundle:LessonTemplate')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find LessonTemplate entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('lessontemplate_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a LessonTemplate entity.
     *
     * @Route("/{id}", name="lessontemplate_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('MetafloSchemoBundle:LessonTemplate')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find LessonTemplate entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('lessontemplate'));
    }

    /**
     * Creates a form to delete a LessonTemplate entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('lessontemplate_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
