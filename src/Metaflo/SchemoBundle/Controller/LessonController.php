<?php

namespace Metaflo\SchemoBundle\Controller;

use Metaflo\SchemoBundle\Entity\Project;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Metaflo\SchemoBundle\Entity\Lesson;
use Metaflo\SchemoBundle\Form\LessonType;

/**
 * Lesson controller.
 *
 * @Route("{id}/lesson")
 */
class LessonController extends Controller {

//    /**
//     * Lists all Lesson entities.
//     *
//     * @Route("/", name="lesson")
//     * @Method("GET")
//     * @Template()
//     */
//    public function indexAction() {
//        $em = $this->getDoctrine()->getManager();
//
//        $entities = $em->getRepository('MetafloSchemoBundle:Lesson')->findAll();
//
//        return array(
//            'entities' => $entities,
//        );
//    }

    /**
     * Creates a new Lesson entity.
     *
     * @Route("/", name="lesson_create")
     * @Method("POST")
     * @Template("MetafloSchemoBundle:Lesson:new.html.twig")
     */
    public function createAction(Request $request, $id) {
        $entity = new Lesson();
        $project = $this->getProject($id);
        $entity->setProject($project);
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $year = $project->getSchedule()->getDateStart()->format("Y");
            $weekStart = $project->getSchedule()->getDateStart()->format("W");
            $weekEnd = $project->getSchedule()->getDateEnd()->format("W");
            //TODO: can't handle a year switch right now - improve!
            $weekDay = new \DateTime();
            for ($i = $weekStart; $i <= $weekEnd; $i++) {
                $lessonInstance = clone $entity;
                $weekDay->setISODate(
                        $year,
                        $i, //weekNo
                        $entity->getTimeslot()->getWeekday()
                );

                if ($weekDay >= $project->getSchedule()->getDateStart() &&
                    $weekDay <= $project->getSchedule()->getDateEnd()
                ) {

                    $lessonInstance->setWeekNo($i);
                    $lessonInstance->setYear($year);
                    $em->persist($lessonInstance);
                }
            }

            $em->flush();

            return $this->redirect($this->generateUrl('schedule', array("id" => $id)));
        }

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Lesson entity.
     *
     * @param Lesson $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private
    function createCreateForm(
        Lesson $entity
    ) {
        $form = $this->createForm(
                     new LessonType(),
                     $entity,
                     array(
                         'action' => $this->generateUrl(
                                          'lesson_create',
                                          array(
                                              "id" => $entity->getProject()->getId()
                                          )
                             ),
                         'method' => 'POST',
                     )
        );
        $form->remove("cancelled");
        $form->remove("weekNo");
        $form->remove("year");
        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Lesson entity.
     *
     * @Route("/new", name="lesson_new")
     * @Method("GET")
     * @Template()
     */
    public
    function newAction(
        $id
    ) {
        $entity = new Lesson();
        $entity->setProject($this->getProject($id));
        $form = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Finds and displays a Lesson entity.
     *
     * @Route("/{lid}", name="lesson_show")
     * @Method("GET")
     * @Template()
     */
    public
    function showAction(
        $id,
        $lid
    ) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MetafloSchemoBundle:Lesson')->find($lid);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Lesson entity.');
        }

        $deleteForm = $this->createDeleteForm($lid);

        return array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Lesson entity.
     *
     * @Route("/{lid}/edit", name="lesson_edit")
     * @Method("GET")
     * @Template()
     */
    public
    function editAction(
        $id,
        $lid
    ) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MetafloSchemoBundle:Lesson')->find($lid);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Lesson entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($lid);

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Creates a form to edit a Lesson entity.
     *
     * @param Lesson $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private
    function createEditForm(
        Lesson $entity
    ) {
        $form = $this->createForm(
                     new LessonType(),
                     $entity,
                     array(
                         'action' => $this->generateUrl('lesson_update', array('id' => $entity->getId())),
                         'method' => 'PUT',
                     )
        );

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }

    /**
     * Edits an existing Lesson entity.
     *
     * @Route("/{lid}", name="lesson_update")
     * @Method("PUT")
     * @Template("MetafloSchemoBundle:Lesson:edit.html.twig")
     */
    public
    function updateAction(
        Request $request,
        $id,
        $lid
    ) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MetafloSchemoBundle:Lesson')->find($lid);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Lesson entity.');
        }

        $deleteForm = $this->createDeleteForm($lid);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('lesson_edit', array('lid' => $lid)));
        }

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Lesson entity.
     *
     * @Route("/{lid}", name="lesson_delete")
     * @Method("DELETE")
     */
    public
    function deleteAction(
        Request $request,
        $id,
        $lid
    ) {
        $form = $this->createDeleteForm($lid);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('MetafloSchemoBundle:Lesson')->find($lid);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Lesson entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('lesson'));
    }

    /**
     * Creates a form to delete a Lesson entity by id.
     *
     * @param integer $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private
    function createDeleteForm(
        $id
    ) {
        return $this->createFormBuilder()
                    ->setAction($this->generateUrl('lesson_delete', array('id' => $id)))
                    ->setMethod('DELETE')
                    ->add('submit', 'submit', array('label' => 'Delete'))
                    ->getForm();
    }

    /**
     * @param integer $id
     * @return Project
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    private
    function getProject(
        $id
    ) {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('MetafloSchemoBundle:Project')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Project entity.');
        }

        return $entity;
    }

//    private function getFirstLastScheduleWeek($id) {
//        $schedule = $this->getProject($id)->getSchedule();
//        $weekStart = $schedule->getDateStart()->format("W");
//        $weekEnd = $schedule->getDateEnd()->format("W");
//        return [
//            "weekStart" => $weekStart;
//        ];
//    }
}
