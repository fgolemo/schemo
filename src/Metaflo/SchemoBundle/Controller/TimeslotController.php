<?php

namespace Metaflo\SchemoBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Metaflo\SchemoBundle\Entity\Timeslot;
use Metaflo\SchemoBundle\Form\TimeslotType;

/**
 * Timeslot controller.
 *
 * @Route("/timeslot")
 */
class TimeslotController extends Controller
{

    /**
     * Lists all Timeslot entities.
     *
     * @Route("/", name="timeslot")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('MetafloSchemoBundle:Timeslot')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Timeslot entity.
     *
     * @Route("/", name="timeslot_create")
     * @Method("POST")
     * @Template("MetafloSchemoBundle:Timeslot:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Timeslot();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('timeslot_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a Timeslot entity.
    *
    * @param Timeslot $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Timeslot $entity)
    {
        $form = $this->createForm(new TimeslotType(), $entity, array(
            'action' => $this->generateUrl('timeslot_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Timeslot entity.
     *
     * @Route("/new", name="timeslot_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Timeslot();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Timeslot entity.
     *
     * @Route("/{id}", name="timeslot_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MetafloSchemoBundle:Timeslot')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Timeslot entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Timeslot entity.
     *
     * @Route("/{id}/edit", name="timeslot_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MetafloSchemoBundle:Timeslot')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Timeslot entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Timeslot entity.
    *
    * @param Timeslot $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Timeslot $entity)
    {
        $form = $this->createForm(new TimeslotType(), $entity, array(
            'action' => $this->generateUrl('timeslot_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Timeslot entity.
     *
     * @Route("/{id}", name="timeslot_update")
     * @Method("PUT")
     * @Template("MetafloSchemoBundle:Timeslot:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MetafloSchemoBundle:Timeslot')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Timeslot entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('timeslot_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Timeslot entity.
     *
     * @Route("/{id}", name="timeslot_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('MetafloSchemoBundle:Timeslot')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Timeslot entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('timeslot'));
    }

    /**
     * Creates a form to delete a Timeslot entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('timeslot_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
