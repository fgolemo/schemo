<?php

namespace Metaflo\SchemoBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Metaflo\SchemoBundle\Entity\Lesson;
use Metaflo\SchemoBundle\Entity\Modification;
use Metaflo\SchemoBundle\Entity\Project;
use Metaflo\SchemoBundle\Entity\Schedule;
use Metaflo\SchemoBundle\Form\ModificationType;
use Metaflo\SchemoBundle\Reasoner\Checker;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Config\Definition\Exception\InvalidConfigurationException;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Exception\InvalidParameterException;
use Symfony\Component\Routing\Exception\MissingMandatoryParametersException;
use Metaflo\SchemoBundle\Form\ScheduleType;

/**
 * Schedule controller.
 *
 * @Route("/{id}/schedule")
 */
class ScheduleController extends Controller {
    /**
     * Schedule overview.
     *
     * @Route("/", name="schedule")
     * @Method("GET")
     * @Template()
     */
    public function indexAction($id) {
        $em = $this->getDoctrine()->getManager();
        /** @var Project $entity */
        $entity = $em->getRepository('MetafloSchemoBundle:Project')->find($id);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Project entity.');
        }
        $eventSources = array();
        foreach ($entity->getSchoolgroups() as $schoolgroup) {
            $eventSources[] = array(
                "url" => $this->generateUrl(
                              'schedule_caldata',
                              array(
                                  'id' => $entity->getId(),
                                  'sgID' => $schoolgroup->getId()
                              )
                    ),
                "color" => $this->genColorCodeFromText($schoolgroup->getName(), 150),
                "textColor" => "black"
            );
        }


        return array(
            'eventSources' => $eventSources,
            'entity' => $entity,
            'config' => $this->container->getParameter("metaflo_schemo.reasoningTree"),
            'endpoints' => $this->container->getParameter("metaflo_schemo.reasoningEndpoints"),
        );
    }

    /**
     * Schedule creation.
     *
     * @Route("/generate", name="schedule_generate")
     * @Method("GET")
     * @Template()
     */
    public function generateAction($id) {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('MetafloSchemoBundle:Project')->find($id);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Project entity.');
        }

        return array(
            'entity' => $entity,
            'workerURL' => $this->generateUrl(
                                'schedule_generate_worker',
                                array('id' => $entity->getId())
                )
        );
    }

    /**
     * Schedule generator process.
     *
     * @Route("/generate/worker", name="schedule_generate_worker")
     * @Method("GET")
     */
    public function workerAction($id, Request $request) {
        sleep(1);

        $nextStep = $request->query->get('nextStep');

        $output = array(
            "update" => "Demo process update (like step in a multi-step process) #$nextStep",
            "status" => ($nextStep == 3 ? "finished" : "working"),
            "nextStep" => $nextStep + 1
        );

        $session = new Session();
        $session->start();
        $session->set('schemo.status', 'Drak');

        return new JsonResponse($output);
    }

    /**
     * Schedule generator process.
     *
     * @Route("/caldata/{sgID}", name="schedule_caldata")
     * @Method("GET")
     */
    public function caldataAction($id, $sgID, Request $request) {
        $start = $request->query->get('start');
        $end = $request->query->get('end');
        if (empty($start) || empty($end)) {
            throw new MissingMandatoryParametersException("we need start and end date parameters");
        }

        $em = $this->getDoctrine()->getManager();
        /** @var Lesson[] $lessons */
        $lessons = $em->getRepository('MetafloSchemoBundle:Schedule')
                      ->getCalData($id, $sgID, $start, $end);
        $output = array();
        foreach ($lessons as $lesson) {
            /** @var Lesson $lesson */
            $date = new \DateTime();
            $date->setISODate(
                 $lesson->getYear(),
                 $lesson->getWeekNo(),
                 $lesson->getTimeslot()->getWeekday()
            );
            $outputLine = array(
                "title" => $lesson->getTemplate()->getName() . " " .
                    $lesson->getTemplate()->getSchoolgroup()->getName() . " " .
                    $lesson->getRoom()->getName() . " " .
                    $lesson->getTeacher()->getName(),
                "start" => $date->format("Y-m-d ") .
                    $lesson->getTimeslot()->getTimeStart()->format("H:m:s"),
                "end" => $date->format("Y-m-d ") .
                    $lesson->getTimeslot()->getTimeEnd()->format("H:m:s")
            );
            if ($lesson->getCancelled()) {
                $outputLine["textColor"] = "#f00";
                $outputLine["title"] = "(CANCELLED) ".$outputLine["title"];
            }
            $output[] = $outputLine;
        }

        return new JsonResponse($output);

    }

    /**
     * Outputs a color (#000000) based Text input
     *
     * @param $text String of text
     * @param $min_brightness Integer between 0 and 255
     * @param $spec Integer between 2-10, determines how unique each color will be
     * @throws \Exception
     * @return string
     */
    private function genColorCodeFromText($text, $min_brightness = 100, $spec = 10) {
        if (!is_int($min_brightness)) {
            throw new \Exception("$min_brightness is not an integer");
        }
        if (!is_int($spec)) {
            throw new \Exception("$spec is not an integer");
        }
        if ($spec < 2 or $spec > 10) {
            throw new \Exception("$spec is out of range");
        }
        if ($min_brightness < 0 or $min_brightness > 255) {
            throw new \Exception("$min_brightness is out of range");
        }
        $hash = md5($text); //Gen hash of text
        $colors = array();
        for ($i = 0; $i < 3; $i++) {
            $colors[$i] = max(
                array(
                    round(
                        ((hexdec(substr($hash, $spec * $i, $spec))) /
                            hexdec(str_pad('', $spec, 'F'))) * 255
                    ),
                    $min_brightness
                )
            );
        } //convert hash into 3 decimal values between 0 and 255
        if ($min_brightness > 0) //only check brightness requirements if min_brightness is about 100
        {
            while (array_sum($colors) / 3 < $min_brightness) //loop until brightness is above or equal to min_brightness
            {
                for ($i = 0; $i < 3; $i++) {
                    $colors[$i] += 10;
                }
            }
        } //increase each color by 10
        $output = '';
        for ($i = 0; $i < 3; $i++) {
            $output .= str_pad(dechex($colors[$i]), 2, 0, STR_PAD_LEFT);
        } //convert each color to hex and append to output
        return '#' . $output;
    }

    /**
     * Displays a form to edit an existing Project entity.
     *
     * @Route("/edit", name="schedule_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id) {
        $em = $this->getDoctrine()->getManager();

        /** @var Project $entity */
        $entity = $em->getRepository('MetafloSchemoBundle:Project')->find($id);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Project entity.');
        }
        $schedule = $entity->getSchedule();
        $editForm = $this->createEditForm($schedule);

        return array(
            'entity' => $schedule,
            'edit_form' => $editForm->createView(),
        );
    }

    /**
     * Creates a form to edit a Project entity.
     *
     * @param Schedule $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Schedule $entity) {
        $form = $this->createForm(
                     new ScheduleType(),
                     $entity,
                     array(
                         'action' => $this->generateUrl(
                                          'schedule_update',
                                          array(
                                              'id' => $entity
                                                      ->getProject()
                                                      ->getId()
                                          )
                             ),
                         'method' => 'PUT',
                     )
        );

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }

    /**
     * Edits an existing Project entity.
     *
     * @Route("/", name="schedule_update")
     * @Method("PUT")
     * @Template("MetafloSchemoBundle:Schedule:edit.html.twig")
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();
        /** @var Project $entity */
        $entity = $em->getRepository('MetafloSchemoBundle:Project')->find($id);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Project entity.');
        }
        $schedule = $entity->getSchedule();

        $editForm = $this->createEditForm($schedule);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {

            //TODO: update lessons to be in the new range (i.e. delete the out of-range ones and
            // create new ones)

            $em->flush();

            return $this->redirect($this->generateUrl('schedule_edit', array('id' => $id)));
        }

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
        );
    }

    /**
     * Schedule modification.
     *
     * @Route("/modify", name="schedule_modify")
     * @Method({"GET", "PUT"})
     * @Template()
     */
    public function modifyAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();
        /** @var Project $entity */
        $entity = $em->getRepository('MetafloSchemoBundle:Project')->find($id);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Project entity.');
        }
        $mod = new Modification();
        $mod->setProject($entity);
        $form = $this->createForm(
                     new ModificationType(),
                     $mod,
                     array(
                         'action' => $this->generateUrl(
                                          'schedule_modify',
                                          array('id' => $entity->getId())
                             ),
                         'method' => 'PUT',
                     )
        );
        $form->add('submit', 'submit', array('label' => 'next'));

        $form->handleRequest($request);

        if ($form->isValid()) {
            $lessons = $em->getRepository('MetafloSchemoBundle:Schedule')->getLesson(
                          $id,
                          $mod->getTeacher()->getId(),
                          $mod->getWeek(),
                          $mod->getTimeslot()->getId()
            );
            if (count($lessons) == 0) {
                $form->addError(new FormError("This teacher has no lesson at that day and timeslot"));
            } else {
                return $this->redirect(
                            $this->generateUrl(
                                 'schedule_modify2',
                                 array(
                                     'id' => $id,
                                     'lesson' => $lessons[0]->getId()
                                 )
                            )
                );
            }
        }

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Schedule modification part 2.
     *
     * @Route("/modify2/{lesson}", name="schedule_modify2")
     * @Method("GET")
     * @Template()
     */
    public function modify2Action($id, $lesson) {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('MetafloSchemoBundle:Project')->find($id);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Project entity.');
        }

        return array(
            'entity' => $entity,
            'workerURL' => $this->generateUrl(
                                'schedule_modify_worker',
                                array(
                                    'id' => $entity->getId(),
                                    'lesson' => $lesson,
                                )
                )
        );
    }

    /**
     * Schedule modification ajax operator.
     *
     * @Route("/modifyWorker/{lesson}", name="schedule_modify_worker")
     * @Method("GET")
     * @Template()
     */
    public function modifyWorkerAction(Request $request, $id, $lesson) {
        $em = $this->getDoctrine()->getManager();
        $config = $this->container->getParameter("metaflo_schemo.reasoningTree");
        $endpoints = $this->container->getParameter("metaflo_schemo.reasoningEndpoints");
        $reasoner = new Checker($em, $config, $endpoints, $lesson);
        $step = $request->query->get('step');
        if (empty($step)) {
            $step = "start";
        }
        $feedbackFunction = $request->query->get('feedbackFunction');
        if (empty($feedbackFunction)) {
            $checkFunction = $config[$step]["checkFunction"];
            if (method_exists($reasoner, $checkFunction)) {
                $output = $reasoner->$checkFunction();
                $output["step"] = $step;

                return new JsonResponse($output);
            } else {
                throw new InvalidConfigurationException("ReasonerException: checkFunction '$checkFunction'
                couldn't be found in Reasoner/Checker.");
            }
        } else {
            if (method_exists($reasoner, $feedbackFunction)) {
                $input = $request->query->get('answer');
                $answer = $reasoner->$feedbackFunction($input);
                $reason = $answer["reason"];
                $answer = $answer["answer"];
                $question = $config[$step]["question"];
                $nextStep = $config[$step]["response"][($answer ? "trueStep" : "falseStep")];

                $endMsg = $end = false;
                if (array_key_exists($nextStep, $endpoints)) {
                    $endMsg = $endpoints[$nextStep]["output"];
                    $end = true;
                }

                $output = array(
                    "question" => $question,
                    "answer" => $answer?1:0,
                    "reason" => $reason,
                    "step" => $nextStep,
                    "end" => $end?1:0,
                    "endMsg" => $endMsg
                );

                return new JsonResponse($output);
            } else {
                throw new InvalidParameterException("ReasonerException: feedbackFunction '$feedbackFunction'
                couldn't be found in Reasoner/Checker.");
            }

        }


//        $output = array(
//            "update" => "Demo process update (like step in a multi-step process) #$nextStep",
//            "status" => ($nextStep == 3 ? "finished" : "working"),
//            "nextStep" => $nextStep + 1
//        );
//
//        $session = new Session();
//        $session->start();
//        $session->set('schemo.status', 'Drak');

        $output = array();

        return new JsonResponse($output);
    }
}
