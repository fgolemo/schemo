<?php

namespace Metaflo\SchemoBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Metaflo\SchemoBundle\Entity\Schoolgroup;
use Metaflo\SchemoBundle\Form\SchoolgroupType;

/**
 * Schoolgroup controller.
 *
 * @Route("/schoolgroup")
 */
class SchoolgroupController extends Controller
{

    /**
     * Lists all Schoolgroup entities.
     *
     * @Route("/", name="schoolgroup")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('MetafloSchemoBundle:Schoolgroup')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Schoolgroup entity.
     *
     * @Route("/", name="schoolgroup_create")
     * @Method("POST")
     * @Template("MetafloSchemoBundle:Schoolgroup:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Schoolgroup();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('schoolgroup_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a Schoolgroup entity.
    *
    * @param Schoolgroup $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Schoolgroup $entity)
    {
        $form = $this->createForm(new SchoolgroupType(), $entity, array(
            'action' => $this->generateUrl('schoolgroup_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Schoolgroup entity.
     *
     * @Route("/new", name="schoolgroup_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Schoolgroup();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Schoolgroup entity.
     *
     * @Route("/{id}", name="schoolgroup_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MetafloSchemoBundle:Schoolgroup')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Schoolgroup entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Schoolgroup entity.
     *
     * @Route("/{id}/edit", name="schoolgroup_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MetafloSchemoBundle:Schoolgroup')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Schoolgroup entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Schoolgroup entity.
    *
    * @param Schoolgroup $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Schoolgroup $entity)
    {
        $form = $this->createForm(new SchoolgroupType(), $entity, array(
            'action' => $this->generateUrl('schoolgroup_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Schoolgroup entity.
     *
     * @Route("/{id}", name="schoolgroup_update")
     * @Method("PUT")
     * @Template("MetafloSchemoBundle:Schoolgroup:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MetafloSchemoBundle:Schoolgroup')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Schoolgroup entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('schoolgroup_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Schoolgroup entity.
     *
     * @Route("/{id}", name="schoolgroup_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('MetafloSchemoBundle:Schoolgroup')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Schoolgroup entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('schoolgroup'));
    }

    /**
     * Creates a form to delete a Schoolgroup entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('schoolgroup_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
