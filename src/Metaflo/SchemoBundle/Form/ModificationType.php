<?php

namespace Metaflo\SchemoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ModificationType extends AbstractType {
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add(
            'teacher',
            'entity',
            array(
                'class' => 'MetafloSchemoBundle:Teacher',
                'expanded' => true,
            )
            )
            ->add("week", 'choice', array(
                            'choices'   => array(/*(date("W")-1) => 'last week',*/ //TODO: this is really crappy and last-minute. Fix!
						 (date("W")+0) => 'this week',
                                                 (date("W")+1) => 'next week'),
                        ))
            ->add("timeslot",
                  'entity',
                  array(
                      'class' => 'MetafloSchemoBundle:Timeslot',
                      'expanded' => false,
                  )
            );
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(
                 array(
                     'data_class' => 'Metaflo\SchemoBundle\Entity\Modification'
                 )
        );
    }

    /**
     * @return string
     */
    public function getName() {
        return 'metaflo_schemobundle_modification';
    }
}
