<?php

namespace Metaflo\SchemoBundle\Form;

use Metaflo\SchemoBundle\Entity\Room;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class RoomType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('usageType', 'choice', array(
                                 'choices'   => array(Room::USAGE_CLASS => 'for a class',
                                                      Room::USAGE_ACTIVITY => 'for an activity'),
                             ))
            ->add('usageName')
            ->add('name')
            ->add('project')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Metaflo\SchemoBundle\Entity\Room'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'metaflo_schemobundle_room';
    }
}
