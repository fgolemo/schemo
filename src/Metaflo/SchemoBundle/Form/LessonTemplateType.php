<?php

namespace Metaflo\SchemoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class LessonTemplateType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('roomTypical')
            ->add('teachers')
            ->add('schoolgroup')
            ->add('project')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Metaflo\SchemoBundle\Entity\LessonTemplate'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'metaflo_schemobundle_lessontemplate';
    }
}
