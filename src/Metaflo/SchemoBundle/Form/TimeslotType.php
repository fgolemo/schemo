<?php

namespace Metaflo\SchemoBundle\Form;

use Metaflo\SchemoBundle\Entity\Timeslot;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TimeslotType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('weekday', 'choice', array('choices'   => Timeslot::$DAYS))
            ->add('timeStart')
            ->add('timeEnd')
            ->add('project')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Metaflo\SchemoBundle\Entity\Timeslot'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'metaflo_schemobundle_timeslot';
    }
}
