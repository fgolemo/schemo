function makeCal(cals) {
    cals = JSON.parse(cals);
//    console.log(cals);
    $('#calendar').fullCalendar({
        weekends: false,
        firstDay: 1,
        defaultView: 'agendaWeek',
        eventSources: cals
    });
}

function setupReasoner(url, outputArea, inputArea) {
//    console.log(url);
    reasonStep(url, outputArea, inputArea, null);
}

function reasonStep(url, outputArea, inputArea, inputData) {
    $.getJSON(url, inputData,function (data) {
        if (data.feedbackFunction) {
            var forwardData = {
                "feedbackFunction": data.feedbackFunction,
                "step": data.step
            };
            if (data.input) {
                $(inputArea).html("");
                if (data.input.type == "radio") {
                    $("<p>" + data.input.text + "</p>").appendTo(inputArea);
                    $('<input type="radio" name="reasonChoice" value="0">' +
                        data.input.options[0] + "<br/>").appendTo(inputArea);
                    $('<input type="radio" name="reasonChoice" value="1">' +
                        data.input.options[1] + "<br/>").appendTo(inputArea);
                    $('<button type="submit" class="btn btn-success">next</button>').
                        appendTo(inputArea).click(function () {
                            var answer = $('input[name=reasonChoice]:checked').val();
                            forwardData["answer"] = answer;
                            $(inputArea).html("");
                            return reasonStep(url, outputArea, inputArea, forwardData);
                        });
                }
            } else {
                $(inputArea).html("");
                $("<p>no input needed</p>").appendTo(inputArea);
                return reasonStep(url, outputArea, inputArea, forwardData);
            }
        } else {
            $("<p><strong>"+data.question+":</strong> "+(data.answer?"YES":"NO")+
                '<span class="text-muted"> ('+data.reason+')</span></p>').appendTo(outputArea);
            if (data.end !== 0) {
                $('<div class="alert alert-success"><strong>Result:</strong> '+data.endMsg+'</div>').appendTo(outputArea);
            } else {
                return reasonStep(url, outputArea, inputArea, {"step": data.step});
            }
        }

    }).fail(function () {
        $('<div class="alert alert-danger"><strong>Ah, dammit, something failed...</strong> couldn\'t finish the process. ' +
            'Please check console output.</div>').appendTo(outputArea);
    });
}

function startGenerator(url, outputArea, input) {
    if (!input)
        input = {nextStep: 0};
    $.getJSON(url, input,function (data) {
        $(outputArea).append("\n" + data.update);
        if (data.status && data.status != "finished")
            startGenerator(url, outputArea, {nextStep: data.nextStep});
        else {
            $(outputArea).append("\n" + "Process succeeded. Now go back and view you calendar.");
        }
    }).fail(function () {
        $(outputArea).append("\nDammit, something failed. Sorry! Exiting.");
    });
}

$(document).ready(function () {
    //makeCal();
});