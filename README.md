Schemo School Scheduling application, based on Symfony 2
========================

If you have no idea what this project is about, watch the
[TRAILER.mp4](https://bitbucket.org/fgolemo/schemo/src/master/TRAILER.mp4?at=master).


0) Requirements
----------------------------------

A full LAMPP stack is recommended. But only a random webserver and PHP 5.5 + MySQL are necessary.
A ready-made Package including Apache webserver, PHP, MySQL and configuration can be downloaded
at [apachefriends.org](http://www.apachefriends.org/de/index.html).



1) Installing
----------------------------------

Download this project, either as zip file or clone the repository with git clone
and the link on top of this page:

    git clone git@bitbucket.org:fgolemo/schemo.git


2) Checking your System Configuration
-------------------------------------

Before starting coding, make sure that your local system is properly
configured for Symfony.

Change into the newly created project folder and
execute the `check.php` script from the command line:

    php app/check.php

The script returns a status code of `0` if all mandatory requirements are met,
`1` otherwise.

Access the `config.php` script from a browser:

    http://localhost/[path/to/symfony/app]/web/config.php

If you get any warnings or recommendations, fix them before moving on.
It is important to keep the "web" folder in the url.

3) Setting up the DB
--------------------
First open the DB admin tool you like the most (like phpMyAdmin) and create a new database.
Assume we name it `schemo` and create a user `schemo` who has
unrestricted rights to this very db. No open the file

    app/config/parameters.yml

...and fill in the details for the db you just created.

If you want to have the **demo data**, you can open phpMyAdmin again and import the file
`DB-DUMP.sql.gz` which resides in the project root folder. Note that the including schedule
was only created for **January 2014**. So in the calendar view you might have to lick back
a few weeks till you see the actual events.

4) Running the thing
--------------------

If you want to start the app, point your browser to

    http://localhost/[schemo, or wherever you installed it]/web/app_dev.php

You should see a list of projects (or an empty list) and be able to "view" the first project
to get to the demo data.

